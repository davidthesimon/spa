# David Simon's SPA

Based from - The default Gatsby starter.

For an overview of the project structure please refer to the [Gatsby documentation - Building with Components](https://www.gatsbyjs.org/docs/building-with-components/).

## Install

Make sure that you have the Gatsby CLI program installed:
```sh
npm install --global gatsby-cli
```

Make sure all dependencies are installed
```sh
npm install
```

Then you can run/develop it by:
```sh
gatsby develop
```

Compile/build the website using:
```sh
gatsby build
```
This will generate static files into /public ready for the live server

## Notes

Reactstrap https://reactstrap.github.io/ is used for Bootstrap 4 components 

Bootstrap 4 css in included under /src/stylesheets/vendor

Font-awesome is not added to npm so it is pulled from cdn library. I have ran out of time...
