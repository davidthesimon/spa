import React, {Component} from 'react';
import Layout from 'components/layout/Layout';
import ImageWithTextBox from 'components/reusable/ImageWithTextBox'
import GreyBoxes from 'components/reusable/GreyBoxes'
import Carousel from 'components/Carousel'
import first from 'images/first.jpg';
import second from 'images/second.jpg';
import third from 'images/third.jpg';

class IndexPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            first: {
                id: "section-1",
                imageAlt: "First image",
                bgColor: "white",
                boxBgColor: "orange",
                copyColor: "white",
                title: "<h2 class='title'>Lorem ipsum dolor sit ame elit.</h2>",
                copy: `<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur risus ac eros congue posuere. Duis placerat consequat elit quis.</p>`
            },
            second: {
                id: "section-2",
                imageAlt: "Second image",
                bgColor: "gray-light",
                boxBgColor: "gray",
                copyColor: "white",
                title: "<h2 class='title'>Lorem ipsum dolor sit ame elit.</h2>",
                copy: `<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur risus ac eros congue posuere. Duis placerat consequat elit quis.</p>`
            },
            third: {
                id: "section-4",
                imageAlt: "Third image",
                bgColor: "white",
                boxBgColor: "gray-light",
                copyColor: "black",
                title: "<h2 class='title'>Lorem ipsum dolor sit ame elit.</h2>",
                copy: `<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur risus ac eros congue posuere. Duis placerat consequat elit quis.</p>`
            },
            greyBoxes: [
                {
                    title: "Lorem ipsum dolor",
                    copy: `<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur risus ac eros congue posuere. Duis placerat consequat elit quis.</p>`,
                    links: [
                        {to: 'https://www.google.co.uk/', text: 'Lorem ipsum'},
                        {to: 'https://www.google.co.uk/', text: 'Ipsum lorem'}
                    ]
                },
                {
                    title: "Lorem ipsum dolor",
                    copy: `<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur risus ac eros congue posuere. Duis placerat consequat elit quis.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur risus ac eros congue posuere. Duis placerat consequat elit quis.</p>`,
                    links: [
                        {to: 'https://www.google.co.uk/', text: 'Find out more in FAQs'}
                    ]
                },
                {
                    title: "Lorem ipsum dolor",
                    copy: `<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur risus ac eros congue posuere. Duis placerat consequat elit quis.</p>`,
                    links: [
                        {to: 'https://www.google.co.uk/', text: 'Find out more in FAQs'},
                        {to: 'https://www.google.co.uk/', text: 'Find out more in FAQs'}
                    ]
                }
            ]
        }
    }

    render() {
        return (
            <Layout>
                <ImageWithTextBox
                    id={this.state.first.id}
                    image={first}
                    imageAlt={this.state.first.imageAlt}
                    title={this.state.first.title}
                    copy={this.state.first.copy}
                    bgColor={this.state.first.bgColor}
                    boxBgColor={this.state.first.boxBgColor}
                    copyColor={this.state.first.copyColor}
                />

                <ImageWithTextBox
                    id={this.state.second.id}
                    image={second}
                    imageAlt={this.state.second.imageAlt}
                    title={this.state.second.title}
                    copy={this.state.second.copy}
                    bgColor={this.state.second.bgColor}
                    boxBgColor={this.state.second.boxBgColor}
                    copyColor={this.state.second.copyColor}
                    textOnLeft={true}
                />

                <Carousel
                    id="section-3"
                />

                <ImageWithTextBox
                    id={this.state.third.id}
                    image={third}
                    imageAlt={this.state.third.imageAlt}
                    title={this.state.third.title}
                    copy={this.state.third.copy}
                    bgColor={this.state.third.bgColor}
                    boxBgColor={this.state.third.boxBgColor}
                    copyColor={this.state.third.copyColor}
                />

                <GreyBoxes
                    id="section-5"
                    items={this.state.greyBoxes}
                />
            </Layout>
        )
    }
}

export default IndexPage
