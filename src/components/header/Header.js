import React, {Component} from 'react';
import {Link} from 'gatsby';
import logo from 'images/logo-small.png';

const scrollToElement = require('scroll-to-element');

class Header extends Component {

    constructor(props) {
        super(props);

        this.state = {
            collapsed: false,
            navToggled: false
        };
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll, true);
        window.addEventListener('resize', this.handleScroll, true);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll, true);
        window.removeEventListener('resize', this.handleScroll, true);
    }

    handleScroll = () => {
        // let scrollPosition = document.documentElement.scrollTop
        let scrollPosition = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);
        let collapsed = scrollPosition < 50 ? false : true;
        this.setState({
            collapsed
        });
    };

    scrollToAnchor = (e, anchor) => {
        e.preventDefault();

        scrollToElement(anchor, {
            duration: 200
        });
    };

    toggleNav = () => {
        this.setState({
            navToggled: !this.state.navToggled
        });
    };

    render() {
        const {collapsed, navToggled} = this.state;

        return (
            <>
                <header className={`header ${collapsed ? 'collapsed' : ''}`}>
                    <div className="header__container">
                        <Link to="/" className="header__logo">
                            <img src={logo} alt="Made up website"/>
                        </Link>
                    </div>
                </header>

                <button
                    className={`nav-toggle ${navToggled ? 'active' : ''} ${collapsed ? 'collapsed' : ''}`}
                    onClick={this.toggleNav}
                    type="button"
                    aria-expanded="false"
                    aria-controls="navbarSupportedContent"
                    aria-label="Toggle navigation"
                >
                    <span className="nav-toggle__burger"></span>
                </button>

                <nav id="navbarSupportedContent"
                     className={`nav ${navToggled ? 'active' : ''} ${collapsed ? 'collapsed' : ''}`}>
                    <ul>
                        <li>
                            <a href="#section-1" onClick={(e) => this.scrollToAnchor(e, '#section-1')}>Section 1</a>
                        </li>
                        <li>
                            <a href="#section-2" onClick={(e) => this.scrollToAnchor(e, '#section-2')}>Section 2</a>
                        </li>
                        <li>
                            <a href="#section-3" onClick={(e) => this.scrollToAnchor(e, '#section-3')}>Section 3</a>
                        </li>
                        <li>
                            <a href="#section-4" onClick={(e) => this.scrollToAnchor(e, '#section-4')}>Section 4</a>
                        </li>
                        <li>
                            <a href="#section-5" onClick={(e) => this.scrollToAnchor(e, '#section-5')}>Section 5</a>
                        </li>
                    </ul>
                </nav>
            </>
        )
    }
}

export default Header
