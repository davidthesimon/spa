import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import Header from 'components/header/Header';
import Footer from 'components/footer/Footer';
import 'stylesheets/main.scss';

const scrollToElement = require('scroll-to-element');

class Layout extends Component {

    componentDidMount() {
        // Scroll to top unless there is an anchor tag in the url
        if (window.location.hash) {
            scrollToElement(window.location.hash, {
                duration: 200
            });
        } else {
            scrollToElement('body', {
                duration: 200
            });
        }
    }

    render() {

        const {children, headData} = this.props;
        return (
            <>
                <Helmet htmlAttributes={
                    {"lang": "en"}
                }>
                    <title>{headData.title}</title>
                    <meta name="description" content={headData.description}/>

                    {/* Facebook tags */}
                    <meta property="og:locale" content="en_GB"/>
                    <meta property="og:type" content="website"/>
                    <meta property="og:title" content={headData.title}/>
                    <meta property="og:description" content={headData.description}/>
                    <meta property="og:url" content="https://www.madeupdomain.com"/>
                    <meta property="og:site_name" content="Made up website"/>
                    <meta property="og:image" content="https://www.madeupdomain.com/images/logo.png"/>

                    {/* Twitter tags */}
                    <meta name="twitter:card" content="summary"/>
                    <meta name="twitter:title" content={headData.title}/>
                    <meta name="twitter:description" content={headData.description}/>

                    {/* Favicon */}
                    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png"/>
                    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png"/>
                    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png"/>

                    <link rel="stylesheet"
                          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"/>
                </Helmet>
                <noscript className="no-js">Javascript is required to view the full experience of this site.</noscript>
                <Header siteTitle={headData.title}/>
                {children}
                <Footer/>
            </>
        )
    }
}

Layout.propTypes = {
    children: PropTypes.node.isRequired,
};


Layout.defaultProps = {
    headData: {
        title: 'Made up website | We don\'t know what this is all about',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur risus ac eros congue posuere. Duis placerat consequat elit quis.',
        googleRecaptcha: false
    }
};

export default Layout
