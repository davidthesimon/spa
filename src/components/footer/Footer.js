import React from 'react';
import {Link} from 'gatsby';
import {Container, Row, Col} from 'reactstrap';
import logo from 'images/logo.png';

var FontAwesome = require('react-fontawesome');

const Footer = () => (
    <footer className="footer">
        <Container>
            <Row>
                <Col sm={12} md={4}>
                    <img src={logo} alt="Made up website" className="footer__logo pb-4"/>
                </Col>
                <Col sm={12} md={4}>
                </Col>
                <Col sm={12} md={4}>
                    <ul className="footer__links footer__links--social">
                        <li>
                            <a href="https://www.linkedin.com/" target="_blank" rel="noopener noreferrer"
                               className="icon icon-linkedin">
                                <FontAwesome name="linkedin"/>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/" target="_blank" rel="noopener noreferrer"
                               className="icon icon-facebook">
                                <FontAwesome name="facebook"/>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/" target="_blank" rel="noopener noreferrer"
                               className="icon icon-twitter">
                                <FontAwesome name="twitter"/>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/" target="_blank" rel="noopener noreferrer"
                               className="icon icon-instagram">
                                <FontAwesome name="instagram"/>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/" target="_blank" rel="noopener noreferrer"
                               className="icon icon-youtube">
                                <FontAwesome name="youtube"/>
                            </a>
                        </li>
                    </ul>
                    <ul className="footer__links">
                        <li>
                            <Link to="/" className="link link--white mb-2">Terms and
                                Conditions</Link>
                        </li>
                        <li>
                            <Link to="/" className="link link--white mb-2">Privacy and Cookies</Link>
                        </li>
                    </ul>
                </Col>
            </Row>
            <Row>
                <Col xs={12}>
                    <p className="footer__copyright">© Made up website | Lorem ipsum dolor sit amet, consectetur
                        adipiscing elit.</p>
                </Col>
            </Row>
        </Container>
    </footer>
);

export default Footer