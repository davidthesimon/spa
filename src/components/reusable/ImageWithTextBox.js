import React, {Component} from 'react'
import {Container, Row, Col} from 'reactstrap'

/**
 * Color options (bgColor, boxBgColor, copyColor)
 *
 *  black:        #000000;
 *  white:        #ffffff;
 *  gray-dark:    #353535;
 *  gray-light:   #f3f3f3;
 *  gray:         #52565a;
 *  orange:       #fd7e14;
 *
 * Properties expected
 *  bgColor (string) // Background colour of the container
 *  boxBgColor (string) // Background colour of the box
 *  copyColor (string) // Colour of the text
 *  textOnLeft (true|false) // Order of image and text
 *
 *  id (string) *required
 *  title (string / html) *required
 *  copy (string / html) *required
 **/

class ImageWithTextBox extends Component {

    constructor(props) {
        super(props)

    };

    render() {
        const bgColor = this.props.bgColor ? 'bg-' + this.props.bgColor : '';
        const boxBgColor = this.props.boxBgColor ? 'bg-' + this.props.boxBgColor : '';
        const copyColor = this.props.copyColor ? 'text-' + this.props.copyColor : '';
        const imageOnLeft = !this.props.textOnLeft ? 'image-on-left' : '';
        const imageAlt = this.props.imageAlt ? this.props.imageAlt : '';

        let leftCol, rightCol, leftColLg, rightColLg

        if (this.props.textOnLeft) {
            leftCol = {size: 6, offset: 0};
            rightCol = {size: 7, offset: 5};

            leftColLg = {size: 6, offset: 0};
            rightColLg = {size: 8, offset: 5};
        } else {
            leftCol = {size: 6, offset: 6};
            rightCol = {size: 7};

            leftColLg = {size: 6, offset: 6};
            rightColLg = {size: 7};
        }

        return (
            <Container fluid={true} className={`imageWithTextBox ${bgColor}`} id={this.props.id}>
                <Container className="imageWithTextBox__container">
                    <Row className="imageWithTextBox__row align-items-center">
                        <Col lg={leftColLg} xl={leftCol} className={`imageWithTextBox__content ${copyColor}`}>
                            <div className={`imageWithTextBox__content-bg ${boxBgColor}`}>
                                <div dangerouslySetInnerHTML={{__html: this.props.title}}/>
                                <div dangerouslySetInnerHTML={{__html: this.props.copy}}/>
                            </div>
                        </Col>
                        <Col lg={rightColLg} xl={rightCol} className={`imageWithTextBox__img ${imageOnLeft}`}>
                            <img src={this.props.image} alt={imageAlt}/>
                        </Col>
                    </Row>
                </Container>
            </Container>
        );
    }
}

export default ImageWithTextBox