import React, {Component} from 'react'
import {Container, Row, Col, Collapse} from 'reactstrap'

/**
 * Properties expected
 *
 *  items = object
 *      - title (string) *required
 *      - copy (string / html)  *required
 *      - links = object
 *          - to (string)
 *          - text (string )
 *
 *  itemCount (int) // (1|2|3)
 *
 *  id (string) *required
 *
 **/

class GreyBoxes extends Component {

    constructor(props) {
        super(props);

        let collapseState = {};

        this.props.items.map((item, i) => {
            collapseState[`collapse${i}`] = false
        });

        this.state = collapseState;

        this.toggleCollapse = this.toggleCollapse.bind(this);
        this.collapseToggleWindowWidth = this.collapseToggleWindowWidth.bind(this);
    }

    componentDidMount() {
        this.collapseToggleWindowWidth();
        window.addEventListener('resize', this.collapseToggleWindowWidth, true);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.collapseToggleWindowWidth, true);
    }

    collapseToggleWindowWidth() {
        const width = window.innerWidth;
        const newState = {};
        let collapsed;

        if (width < 992) {
            collapsed = false;
        } else {
            collapsed = true;
        }

        for (let key in this.state) {
            newState[key] = collapsed;
        }

        this.setState(newState);
    }

    toggleCollapse(e) {
        e.preventDefault();

        const width = window.innerWidth;

        if (width < 992) {
            const collapse = e.currentTarget.id;
            this.setState({
                [collapse]: !this.state[collapse]
            })
        }

    }

    render() {
        let lg;
        const itemCount = this.props.items.length;

        if (itemCount === 1) {
            lg = 12
        } else if (itemCount === 2) {
            lg = 6
        } else {
            lg = 4
        }

        const greyBoxes = this.props.items.map((item, i) => {

            let links = false;

            if (item.links !== undefined && item.links.length > 0) {
                links = item.links.map((link, i) => {
                    return (
                        <li key={i}>
                            <a
                                href={link.to}
                                rel="noopener noreferrer"
                                target="_blank"
                                download={link.download !== undefined && link.download}
                            >
                                {link.text}
                            </a>
                        </li>
                    )
                })
            }

            return (
                <Col xs={11} md={12} lg={lg} className="greyBoxes__item" key={i}>
                    <button
                        id={`collapse${i}`}
                        onClick={this.toggleCollapse}
                        className={`greyBoxes__trigger ${this.state[`collapse${i}`] !== true ? 'active' : ''}`}>
                        <span className="sr-only">Toggle collapse</span>
                    </button>
                    <h3 className="greyBoxes__title title">{item.title}</h3>
                    <Collapse isOpen={this.state[`collapse${i}`]} className="greyBoxes__inner">
                        <div className="greyBoxes__content">
                            <div dangerouslySetInnerHTML={{__html: item.copy}}/>
                            {links &&
                            <ul className="greyBoxes__links">
                                {links}
                            </ul>
                            }
                        </div>
                    </Collapse>
                </Col>
            )
        });


        return (
            <Container fluid={true} className="greyBoxes" id={this.props.id}>
                <Container>
                    <Row>
                        {greyBoxes}
                    </Row>
                </Container>
            </Container>
        )
    }

}

export default GreyBoxes