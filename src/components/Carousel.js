import React, {Component} from 'react'
import Slider from 'react-slick'
import {Container} from 'reactstrap'
import "stylesheets/vendor/slick.scss";
import "stylesheets/vendor/slick-theme.scss";
import first from 'images/carousel/first.jpg'
import second from 'images/carousel/second.jpg'
import third from 'images/carousel/second.jpg'
import fourth from 'images/carousel/first.jpg'

/**
 * Properties expected
 *
 *  id (string) *required
 *
 **/

class Carousel extends Component {

    render() {
        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 2,
            slidesToScroll: 2,
            lazyLoad: true,
            arrows: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
            ]
        };
        return (
            <Container className="container-content-max carousel" id={this.props.id} fluid={true}>
                <Slider {...settings}>
                    <div className="carousel__item">
                        <img src={first} alt="First image"/>
                        <div className="carousel__item-content">
                            <h3 className="title">First image</h3>
                            <p className="carousel__item-content-subline">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit
                            </p>
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur risus ac
                                eros congue posuere. Duis placerat consequat elit quis. Lorem ipsum dolor sit amet,
                                consectetur adipiscing elit. Curabitur consectetur risus ac eros congue posuere. Duis
                                placerat consequat elit quis."</p>
                        </div>
                    </div>
                    <div className="carousel__item">
                        <img src={second} alt="Second image"/>
                        <div className="carousel__item-content">
                            <h3 className="title">Second image</h3>
                            <p className="carousel__item-content-subline">
                                Lorem ipsum dolor sit amet
                            </p>
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur risus ac
                                eros congue posuere. Duis placerat consequat elit quis. Lorem ipsum dolor sit amet,
                                consectetur adipiscing elit. Curabitur consectetur risus ac eros congue posuere. Duis
                                placerat consequat elit quis."</p>
                        </div>
                    </div>
                    <div className="carousel__item">
                        <img src={third} alt="Third image"/>
                        <div className="carousel__item-content">
                            <h3 className="title">Third image</h3>
                            <p className="carousel__item-content-subline">
                                Lorem ipsum dolor sit amet
                            </p>
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur risus ac
                                eros congue posuere. Duis placerat consequat elit quis. Lorem ipsum dolor sit amet,
                                consectetur adipiscing elit. Curabitur consectetur risus ac eros congue posuere. Duis
                                placerat consequat elit quis."</p>
                        </div>
                    </div>
                    <div className="carousel__item">
                        <img src={fourth} alt="Fourth image"/>
                        <div className="carousel__item-content">
                            <h3 className="title">Fourth image</h3>
                            <p className="carousel__item-content-subline">
                                Lorem ipsum dolor sit amet
                            </p>
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur risus ac
                                eros congue posuere. Duis placerat consequat elit quis. Lorem ipsum dolor sit amet,
                                consectetur adipiscing elit. Curabitur consectetur risus ac eros congue posuere. Duis
                                placerat consequat elit quis."</p>
                        </div>
                    </div>
                </Slider>
            </Container>
        );
    }

}

export default Carousel