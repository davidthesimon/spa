module.exports = {
    siteMetadata: {
        title: 'Made up website | We don\'t know what this is all about',
        siteUrl: 'https://www.madeupdomain.com'
    },
    plugins: [
        'gatsby-plugin-resolve-src',
        'gatsby-plugin-react-helmet',
        {
            resolve: `gatsby-plugin-sass`,
            options: {
                precision: 8,
            },
        },
        {
            resolve: `gatsby-plugin-canonical-urls`,
            options: {
                siteUrl: `https://www.madeupdomain.com`,
            },
        },
        `gatsby-plugin-offline`,
    ],
};